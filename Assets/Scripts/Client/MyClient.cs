﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

public class MyClient : MonoBehaviour
{
    [SerializeField] string clientName;
    [SerializeField] GameObject messagePrefab;

    public string ClientName {
        get
        {
            return clientName;
        }
        set
        {
            clientName = value;
        }
    }
    private InputField inputField;
    private GameObject chatWindow;
    /// <summary>
    /// this one should be local IPAdrress
    /// </summary>
    private string defaultHost = "192.168.1.29";
    private int defaultPort = 6321;
    private bool isConnected = false;

    private TcpClient client;
    private NetworkStream networkStream;
    private StreamWriter streamWriter;
    private StreamReader streamReader;
    private void Start()
    {
        inputField = GameObject.Find("SendInput").GetComponent<InputField>();
        chatWindow = GameObject.Find("ChatWindow");
    }
    public void ConnectToServer()
    {
        if (isConnected)
        {
            return;
        }    

        client = new TcpClient(defaultHost, defaultPort);
        networkStream = client.GetStream();
        streamWriter = new StreamWriter(networkStream);
        streamReader = new StreamReader(networkStream);
        isConnected = true;
        Debug.Log("Connecting to Server");
    }
    private void Update()
    {
        //whether something is in the stream
        if (isConnected)
        {
            if (networkStream.DataAvailable)
            {
                string data = streamReader.ReadLine();
                if (data != null)
                {
                    OnIncomingData(data);
                }
            }
        }
    }
    private void OnIncomingData(string data)
    {
        if (data.Contains("%NAME"))
        {
            streamWriter.WriteLine("%NAME|" + ClientName);
            streamWriter.Flush();
        }
        else
        {
            Debug.Log(data);
            GameObject chat = Instantiate(messagePrefab, transform.position, Quaternion.identity);
            chat.GetComponentInChildren<Text>().text = data;
            chat.transform.parent = chatWindow.transform;
        }
    }
    public void SendMessage()
    {
        string data = inputField.text;
        streamWriter.WriteLine(data);
        streamWriter.Flush();
    }
    public void Disconnect()
    {
        if (isConnected)
        {
            return;
        }
        streamReader.Close();
        streamWriter.Close();
        client.Close();
        isConnected = false;
    }
}
