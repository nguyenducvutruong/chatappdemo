﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class MyServer : MonoBehaviour
{
    [SerializeField] int defaultPort = 6321;

    private List<MyServerClient> clients;
    private List<MyServerClient> disconnectedList;
    private TcpListener server;
    private bool serverStarted = false;
    private void Start()
    {
        clients = new List<MyServerClient>();
        disconnectedList = new List<MyServerClient>();

        if (serverStarted)
        {
            return;
        }

        try
        {
            
            server = new TcpListener(IPAddress.Any, defaultPort);
            server.Start();
            serverStarted = true;

            StartListening();
            Debug.Log("Server Started");
        }
        catch(Exception e)
        {
            Debug.LogError("Error: " + e.Message);
        }
    }
    private void Update()
    {
        if (!serverStarted)
        {
            return;
        }

        foreach(MyServerClient client in clients)
        {
            //If the client disconnects
            if (!IsConnected(client.tcp))
            {
                client.tcp.Close();
                disconnectedList.Add(client);
                continue;
            }
            else
            {
                NetworkStream stream = client.tcp.GetStream();
                if (stream.DataAvailable)
                {
                    StreamReader reader = new StreamReader(stream);
                    string data = reader.ReadLine();
                    if (data != null)
                    {
                        OnIncomingData(data, client);
                    }
                }
            }
        }
    }
    private void StartListening()
    {
        server.BeginAcceptTcpClient(AcceptTcpClient, server);
    }

    private void AcceptTcpClient(IAsyncResult ar)
    {
        TcpListener listener = (TcpListener)ar.AsyncState;
        TcpClient listenedClient = listener.EndAcceptTcpClient(ar);
        clients.Add(new MyServerClient(listenedClient));

        StartListening();

        AskForClientName(clients[clients.Count - 1]);
    }
    private void AskForClientName(MyServerClient client)
    {
        string ask = "%NAME";
        StreamWriter writer = new StreamWriter(client.tcp.GetStream());
        writer.WriteLine(ask);
        writer.Flush();
    }
    private void Broadcast(string data, List<MyServerClient> clients)
    {
        foreach(MyServerClient client in clients)
        {
            try
            {
                StreamWriter writer = new StreamWriter(client.tcp.GetStream());
                writer.WriteLine(data);
                writer.Flush();
            }
            catch(Exception e)
            {
                Debug.Log(client.clientName + " has Error: " + e.Message);
            }
        }
    }
    void OnIncomingData(string data, MyServerClient client)
    {
        //The stream containing client name
        if (data.Contains("%NAME|"))
        {
            string name = data.Split('|')[1];
            client.clientName = name;
            Broadcast(client.clientName + " has joined the server", clients);
        }
        //normal chat stream
        else
        {
            string broadcast = client.clientName + ": " + data;
            Broadcast(broadcast, clients);
        }
    }
    private bool IsConnected(TcpClient c)
    {
        try
        {
            if (c != null && c.Client != null && c.Client.Connected)
            {
                if (c.Client.Poll(0, SelectMode.SelectRead))
                {
                    return !(c.Client.Receive(new byte[1], SocketFlags.Peek) == 0);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }
}

public class MyServerClient
{
    public TcpClient tcp;
    public string clientName;
    public MyServerClient(TcpClient client)
    {
        tcp = client;
        clientName = "DefaultName";
    }
}